# WebDsv

## Setup

Clone este repositório. A porta padrão está no arquivo `docker-compose.yml` que é 8080, modifique para sua preferência.

Execute este comando na raiz do projeto:

```bash
make
```

Exemplo da aplicação em funcionamento:

https://validador.lt.coop.br/